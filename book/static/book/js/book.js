$(document).ready(function(){
    noKeywordFound();

    function noMatchFound(){
        var table = $("#search_result");
        table.append("<tr><td colspan='4'>No books in record match the keyword. Try something less specific.</td></tr>");
    }

    function noKeywordFound(){
        var table = $("#search_result");
        table.append("<tr><td colspan='4'>Start searching your favorite book by typing your keyword in the search box.</td></tr>");
    }

    var current_kata_kunci = "";
    $("#keyword").keyup(function(){
        var kata_kunci = $("#keyword").val();
        if (current_kata_kunci !== kata_kunci) {
            current_kata_kunci = kata_kunci;

            var table = $("#search_result");
            table.empty();
            
            if (kata_kunci === "") {
                noKeywordFound();
            } else {
                $.getJSON("/library/book.json?q=" + kata_kunci, function(data){
                    if (data.totalItems == 0) {
                        noMatchFound();
                    } else {
                        var items = data.items;
                        var len = items.length;
                        for (i = 0; i < 10 && i < len; i++) {
                            volumeInfo = items[i].volumeInfo;
            
                            table.append("<tr>");
            
                            if (volumeInfo.hasOwnProperty('imageLinks')) {
                                img_src = volumeInfo.imageLinks.smallThumbnail;
                                table.append("<td><img src='" + img_src + "'></td>");
                            } else {
                                img_txt = "No picture";
                                table.append("<td>" + img_txt + "</td>");
                            }
                            // bisa gini juga ternyata
                            // title = volumeInfo.title || "Unknown Book Title"
            
                            title = "Unknown Book Title";
                            if (volumeInfo.hasOwnProperty('title')) {
                                title = volumeInfo.title;
                            }
                            // title = "</td><script>alert(1)</script><td>"
                            table.append("<td>" + title + "</td>");
            
                            authors = ["Unknown Author(s)"];
                            if (volumeInfo.hasOwnProperty('authors')) {
                                authors = volumeInfo.authors;
                            }
                            table.append("<td>" + authors.toString() + "</td>");
            
                            description = "No Description";
                            if (volumeInfo.hasOwnProperty('description')) {
                                description = volumeInfo.description;
                            }
                            table.append("<td>" + description + "</td>");
            
                            table.append("</tr>");
                        }
                    }
                });
            }
        }
    });
});