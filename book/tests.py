from django.test import TestCase, Client
import json
import requests

# use this to run coverage on local
# coverage run --include="book/*" --omit="manage.py,story_6/*" manage.py test 
# coverage report -m

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/library/book')
        self.assertEquals(response.status_code, 200)

    def test_json_successfully_retrieved(self):
        kata_kunci = 'azzosk1203@ll'
        bookjson = json.loads(Client().get('/library/book.json?q=' + kata_kunci).content)
        googleapi = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + kata_kunci).json()
        self.assertEquals(bookjson, googleapi)