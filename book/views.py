import requests
from django.http import JsonResponse
from django.shortcuts import render, redirect

def book(request):
    return render(request,"book/book.html")
    
def book_json(request):
    context = request.GET
    kata_kunci = context.get("q")
    response = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + kata_kunci)
    data = response.json()
    return JsonResponse(data)