from django.urls import path

from .views import book, book_json

urlpatterns = [
    path('book', book, name='book'),
    path('book.json', book_json, name='book_json'),
]