from django import forms
from .models import Kegiatan, Person

class LoginForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
        widgets = {
            'nama_kegiatan' : forms.TextInput(attrs={
                'class' : 'form-input'
            }),
        }
        