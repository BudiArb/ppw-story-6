from django.test import TestCase, Client

# use this to run coverage on local
# coverage run --include="userhandle/*" --omit="manage.py,story_6/*" manage.py test
# coverage report -m

class UnitTest(TestCase):
    def test_url_login_exist(self):
        response = Client().get('/userhandle/login')
        self.assertEquals(response.status_code, 200)

    def test_url_register_exist(self):
        response = Client().get('/userhandle/register')
        self.assertEquals(response.status_code, 200)

    def test_url_logout_exist(self):
        response = Client().get('/userhandle/logout')
        self.assertEquals(response.status_code, 302)

    def test_berhasil_register(self):
        username = 'tes_nama'
        password = 'tes_pw'
        response = Client().post('/userhandle/register', {'username': username, 'password': password})
        self.assertEquals(response.status_code, 302)

    def test_berhasil_login(self):
        # buat akun dummy
        username = 'tes_nama'
        password = 'tes_pw'
        response = Client().post('/userhandle/register', {'username': username, 'password': password})
        response = Client().get('/userhandle/logout')
        
        response = Client().post('/userhandle/login', {'username': username, 'password': password})
        self.assertEquals(response.status_code, 302)

    def test_gagal_login(self):
        # buat akun dummy
        username = 'tes_nama'
        password = 'tes_pw'
        response = Client().post('/userhandle/register', {'username': username, 'password': password})
        response = Client().get('/userhandle/logout')

        response = Client().post('/userhandle/login', {'username': username, 'password': 'password_salah'})
        self.assertEquals(response.status_code, 200)
        
    def test_berhasil_logout(self):
        # buat akun dummy
        username = 'tes_nama'
        password = 'tes_pw'
        response = Client().post('/userhandle/register', {'username': username, 'password': password})

        response = Client().get('/userhandle/logout')
        self.assertEquals(response.status_code, 302)