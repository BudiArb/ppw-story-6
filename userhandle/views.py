from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.contrib.auth.models import User

def login(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            django_login(request, user)
            # Redirect to a success page.
            return HttpResponseRedirect('/')
        else:
            context = {
                "error_message" : "Username atau password salah."
            }

    return render(request, 'userhandle/login.html', context)

def register(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = User.objects.create_user(username=username, password=password)
        user.save()
        
        django_login(request, user)
        return HttpResponseRedirect('/')

    return render(request, 'userhandle/register.html', context)

def logout(request):
    django_logout(request)
    return HttpResponseRedirect("/")
