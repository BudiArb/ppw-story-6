from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index),
    path('gallery', views.gallery),
    path('story1', views.story1)
]
