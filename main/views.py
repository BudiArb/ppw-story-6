from django.shortcuts import render

def index(request):
    return render(request,"main/index.html")

def gallery(request):
    return render(request,"main/gallery.html")

def story1(request):
    return render(request,"main/story1.html")