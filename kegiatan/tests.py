from django.test import TestCase, Client
from .models import Kegiatan, Person

class Testing(TestCase):
    def test_apakah_url_kegiatan_ada(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)
        
    def test_apakah_url_tambah_kegiatan_ada(self):
        response = Client().get('/kegiatan/tambah-kegiatan')
        self.assertEquals(response.status_code, 200)

    def test_apakah_di_formulir_tambah_kegiatan_ada_judul_dan_button_submit(self):
        response = Client().get('/kegiatan/tambah-kegiatan')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tambah Kegiatan", html_kembalian)
        self.assertIn("Submit", html_kembalian)
        
    def test_apakah_di_formulir_tambah_kegiatan_ada_templatenya(self):
        response = Client().get('/kegiatan/tambah-kegiatan')
        self.assertTemplateUsed(response, 'kegiatan/tambah_kegiatan.html')

    def test_apakah_sudah_ada_model_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan="INI NAMA")
        hitung_berapa_bukunya = Kegiatan.objects.all().count()
        self.assertEquals(hitung_berapa_bukunya, 1)
        
    def test_apakah_sudah_ada_model_person(self):
        Kegiatan.objects.create(nama_kegiatan="TES NAMA")
        Person.objects.create(nama="INI NAMA", kegiatan=Kegiatan.objects.get(nama_kegiatan="TES NAMA"))
        hitung_berapa_bukunya = Person.objects.all().count()
        self.assertEquals(hitung_berapa_bukunya, 1)

    def test_apakah_halaman_index_sudah_menampilkan_data(self):
        Kegiatan.objects.create(nama_kegiatan="TES NAMA")
        response = Client().get('/kegiatan/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("TES NAMA", html_kembalian)
        
    def test_apakah_halaman_tambah_kegiatan_sudah_menyimpan_data_dan_tampilkan_di_halaman_index(self):
        response = Client().post('/kegiatan/tambah-kegiatan', {'nama_kegiatan': 'INI NAMA'})
        self.assertEquals(response.status_code, 302)
        response = Client().get('/kegiatan/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("INI NAMA", html_kembalian)
        