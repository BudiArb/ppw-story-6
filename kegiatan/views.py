from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import KegiatanForm

from .models import Kegiatan, Person

def index(request):
    kegiatan_list = Kegiatan.objects.all()
    person_list = Person.objects.all()
    return render(request, 'kegiatan/index.html', {'kegiatan_list' : kegiatan_list, 'person_list' : person_list})

def tambah_kegiatan(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/kegiatan')
    else:
        form = KegiatanForm()
    return render(request, 'kegiatan/tambah_kegiatan.html', {'form' : form})

def tambah_anggota(request, kegiatan_id):
    kegiatan = Kegiatan.objects.get(id=str(kegiatan_id))
    if request.method == 'POST':
        print(request.POST)
        Person.objects.create(nama=request.POST.get('nama'), kegiatan=kegiatan)
        return HttpResponseRedirect('/kegiatan')
    return render(request, 'kegiatan/tambah_anggota.html', {'kegiatan' : kegiatan})

def hapus_kegiatan(request, kegiatan_id):
    kegiatan = Kegiatan.objects.get(id=str(kegiatan_id))
    kegiatan.delete()
    
    return HttpResponseRedirect('/kegiatan')