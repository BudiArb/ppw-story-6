from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=30)

class Person(models.Model):
    nama = models.CharField(max_length=30);
    kegiatan = models.ForeignKey('Kegiatan', on_delete=models.CASCADE)
