from django.urls import path

from .views import index, tambah_kegiatan, tambah_anggota, hapus_kegiatan

urlpatterns = [
    path('', index, name='index'),
    path('tambah-kegiatan', tambah_kegiatan, name='tambah_kegiatan'),
    path('tambah-anggota/<str:kegiatan_id>', tambah_anggota, name='tambah_anggota'),
    path('hapus/<str:kegiatan_id>', hapus_kegiatan, name='hapus'),
]