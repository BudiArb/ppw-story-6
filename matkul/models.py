from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.
class MataKuliah(models.Model):
    judul_matkul = models.CharField(max_length=30)
    nama_dosen = models.CharField(max_length=25)
    jumlah_sks = models.IntegerField(validators=[MinValueValidator(1)])
    deskripsi = models.CharField(max_length=144)
    semester_tahun = models.CharField(max_length=15)
    ruang_kelas = models.CharField(max_length=10)
