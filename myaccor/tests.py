from selenium import webdriver
from django.test import TestCase, Client, LiveServerTestCase, tag
from django.urls import resolve
from .views import myaccor
from .apps import MyaccorConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# use this to run coverage on local
# coverage run --include="myaccor/*" --omit="manage.py,story_6/*" manage.py test 
# coverage report -m

class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(MyaccorConfig.name, "myaccor")
    def test_urls_is_exist(self):
        response = Client().get('/myaccor/')
        self.assertEqual(response.status_code, 200)
    def test_event_using_template(self):
        response = Client().get('/myaccor/')
        self.assertTemplateUsed(response, 'myaccor/myaccor.html')
    def test_views1_func(self):
        found = resolve('/myaccor/')
        self.assertEqual(found.func, myaccor)
    
@tag('functional')
class FunctionalTest(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    # def test_myaccor_work(self):
    #     self.selenium.get(f'{self.live_server_url}/myaccor')

    #     # inisiasikan tiap element myaccor
    #     element1 = self.selenium.find_element_by_id("head1")
    #     element2 = self.selenium.find_element_by_id("head2")
    #     element3 = self.selenium.find_element_by_id("head3")
    #     element4 = self.selenium.find_element_by_id("head4")
    #     panel_element1 = self.selenium.find_element_by_id("content1")
    #     panel_element2 = self.selenium.find_element_by_id("content2")
    #     panel_element3 = self.selenium.find_element_by_id("content3")  
    #     panel_element4 = self.selenium.find_element_by_id("content4")
        
    #     #membuktikan bahwa jika diklik, maka akan membuka panelnya
    #     element1.click()
    #     self.assertEqual("block", panel_element1.value_of_css_property("display")) 
    #     element2.click()
    #     self.assertEqual("block", panel_element2.value_of_css_property("display")) 
    #     element3.click()
    #     self.assertEqual("block", panel_element3.value_of_css_property("display")) 
    #     element4.click()
    #     self.assertEqual("block", panel_element4.value_of_css_property("display"))

    # def test_myaccor_up(self):
    #     self.selenium.get(f'{self.live_server_url}/myaccor')

    #     #inisiasikan 2 element myaccor yang berurutan
    #     element1 = self.selenium.find_element_by_id("head1")
    #     element2 = self.selenium.find_element_by_id("head2")

    #     #simpan isi element1 awalnya
    #     judul_awal_element1 = element1.find_element_by_class_name("judul").get_attribute('innerHTML')
    #     content_awal_element1 = self.selenium.find_element_by_id("content1").get_attribute('innerHTML')
        
    #     print(content_awal_element1 + " aaa")

    #     #cari tombol up pada element2
    #     up_element2 = self.selenium.find_element_by_id("up2")

    #     #membuktikan bahwa setelah diklik up, isi element1 dan element2 bertukar
    #     up_element2.click()

    #     element2 = self.selenium.find_element_by_id("head2")
    #     judul_akhir_element2 = element2.find_element_by_class_name("judul").get_attribute('innerHTML')
    #     content_akhir_element2 = self.selenium.find_element_by_id("content2").get_attribute('innerHTML')

    #     print(content_awal_element1 + " bbb")
    #     self.assertEqual(content_awal_element1, content_akhir_element2)
    #     self.assertEqual(judul_awal_element1, judul_akhir_element2)
    