from django.urls import path

from .views import myaccor

app_name = 'myaccor'

urlpatterns = [
    path('', myaccor, name="accor"),
]
