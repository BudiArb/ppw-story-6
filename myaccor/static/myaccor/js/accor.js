$(document).ready(function(){
    $(".content").hide();

    $(".head").click(function(){
        var item = $(this);
        $(".accordion-item").each(function(index) {
            if ($(this).children(".head").html() != item.html()) {
                $(this).children(".content").hide();
            }
        });
        $(this).next().toggle();
    });

    $("button").click(function(){
        var this_item = $(this).parents(".accordion-item");
        var other_item;
        
        if ($(this).hasClass("up")) {
            if (this_item.is(":first-child")) other_item = this_item;
            else other_item = this_item.prev();
        } else {
            if (this_item.is(":last-child")) other_item = this_item;
            else other_item = this_item.next();
        }

        var judul1 = this_item.children(".head").children(".judul");
        var isi1 = this_item.children(".content");
        var judul2 = other_item.children(".head").children(".judul");
        var isi2 = other_item.children(".content");
        judul1.html([judul2.html(), judul2.html(judul1.html())][0])
        isi1.html([isi2.html(), isi2.html(isi1.html())][0])

        isi1.show();
    });
});